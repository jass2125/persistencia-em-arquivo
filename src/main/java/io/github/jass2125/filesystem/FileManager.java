/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.github.jass2125.filesystem;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author Anderson Souza
 * @email jair_anderson_bs@hotmail.com
 * @since 2015, Feb 1, 2016
 */
public class FileManager {
    
    
    public static void fileWriter(String path, Pessoa pessoa) throws FileNotFoundException, IOException{
        BufferedWriter bufferReader = new BufferedWriter(new FileWriter(path));
        String line = "";
        bufferReader.append("Nome: " + pessoa.getNome() + "\n");
        bufferReader.append("Idade: " + pessoa.getIdade() + "\n");
        bufferReader.close();
    }

}
